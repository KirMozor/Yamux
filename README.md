# Yamux

## Внимание, это ветка Dev, это означает что здесь git версия проекта, самые свежие фичи (не обязательно доработанные) и баги только для вас :D

**Описание:** Это клиент Яндекс.Музыки для Unix-подобных систем написанный на C# с использованием самописного API ([YandexMusicApi](https://gitlab.com/KirMozor/YandexMusicApi "Репозиторий самого API (C#)"))

## Установка Yamux'а
Пока доступна только сборка программы с исходников.

## Сборка Yamux'а под PC
### Зависимости
* `dotnet-runtime-7.0`, `dotnet-sdk-7.0` (для, неожиданно, сборки самого Yamux'а)

### Установка зависимостей
**Arch GNU/Linux:**
```
sudo pacman -S dotnet-runtime-7.0 dotnet-sdk-7.0
```

**openSUSE Leap 15.5:**

Откройте YaST и добавьте репозиторий dotnet от microsoft
```
https://packages.microsoft.com/opensuse/15/prod/
```
Установите зависимости:
```
sudo zypper install dotnet-sdk-7.0 dotnet-runtime-7.0 
```

**Debian GNU/Linux | Ubuntu Linux:**
```
wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
sudo apt install ./packages-microsoft-prod.deb \
rm packages-microsoft-prod.deb \
sudo apt update; \
sudo apt install -y dotnet-runtime-7.0 dotnet-sdk-7.0
```

### Итак, я установил все нужные зависимости, что дальше?
Дальше, введите следующие команды:

```
git clone https://gitlab.com/Xeronix/Yamux.git
git checkout dev
cd Yamux/Yamux.Desktop
dotnet publish --self-contained -r linux-x64 --configuration Release # или linux-x32 (смотря какая битность у вас)
```
А теперь файл для запуска будет лежать по пути `Yamux/Yamux.Desktop/bin/Release/net7.0/linux-x64/publish/Yamux.Desktop`. Наслаждайтесь!

## Сборка Yamux'а под альтернативные платформы
### Зависимости
* `dotnet-runtime-7.0`, `dotnet-sdk-7.0`, `android-sdk`, `jdk-11-openjdk` (возможно: `jdk-11-openjdk-devel`)

**Arch GNU/Linux:**
```
sudo pacman -S dotnet-runtime-7.0 dotnet-sdk-7.0 jdk11-openjdk 
```

**openSUSE Leap 15.5:**

Откройте YaST и добавьте репозиторий dotnet от microsoft а также репозиторий hardware
```
https://packages.microsoft.com/opensuse/15/prod/
https://download.opensuse.org/repositories/hardware/15.5/
```
Установите зависимости:
```
sudo zypper install dotnet-sdk-7.0 dotnet-runtime-7.0 java-11-openjdk-devel android-tools
```
Вот с установкой Android SDK у вас могут быть проблемы, для корректной установки мне пришлось через JetBrains Rider с плагином Rider Xamarin Android Support скачивать Android SDK (Settings/Build, Execution, Deployment/Android)
* Выберите все пункты из SDK Tools
* Из SDK Platform выбрать:
  * Android API 34
  * Android 13
* Установить

Если есть альтернативный метод то прошу рассказать об этом в Issue или сделать Pull-Request README.

### Итак, я установил все нужные зависимости, что дальше?
Дальше собирите тот вариант что хотели

#### Android:
```
export AndroidSdkDirectory=/opt/android-sdk # или любой другой путь, у меня Android SDK находится в /opt
git clone https://gitlab.com/Xeronix/Yamux.git
git checkout dev
cd Yamux/Yamux.Android
dotnet restore
sudo dotnet workload restore
dotnet publish --self-contained -r linux-x64 --configuration Release # или linux-x32 (смотря какая битность у вас)
```
#### Browser:
```
git clone https://gitlab.com/Xeronix/Yamux.git
git checkout dev
cd Yamux/Yamux.Browser
dotnet restore
sudo dotnet workload restore
dotnet publish --self-contained -r linux-x64 --configuration Release # или linux-x32 (смотря какая битность у вас)
```

### Кстатиии

У меня есть телеграм блог куда я пишу прогресс разработки. Ссылочка внизу, подписывайся!

https://t.me/+-QPSC9v5pNthNTVi

### Используемые библиотеки:
- [Material.Avalonia](https://github.com/AvaloniaCommunity/Material.Avalonia)
- [Material.Icons.Avalonia](https://github.com/AvaloniaUtils/Material.Icons.Avalonia)
- [YandexMusicApi](https://gitlab.com/KirMozor/YandexMusicApi)

### Благодарности:
- Благодарю [Аристарха Бахирева](https://gitlab.com/KirMozor/Yamux/-/blob/45682056e42e4aeac2d9d27c2ffda4828e3db2cd/Yamux/Svg/dark/high-quality.svg) за то что когда-то сделал иконку для выбора качества
- Благодарю [Lameni](https://lameni.tk/ "Lameni - Telegram") за то, что помог мне улучшить README.MD